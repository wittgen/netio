#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "../src/serialization.hpp"
#include "../src/deserialization.hpp"

using namespace netio;


struct buf_source
{
    buf_source() : ctx("posix"), feeder(INIT_BUFFERS, BUFSIZE, &ctx)
    {
        rb = get();
        b = rb->buffer();
    }

    reusable_buffer* get()
    {
        return feeder.pop();
    }

    void append_msg(const char* data, size_t size)
    {
        msgheader header;
        header.len = size;
        b->append((const char*)&header, sizeof(msgheader));
        b->append((const char*)data, size);
    }

    bool all_buffers_released()
    {
        return (feeder.num_available_buffers() == INIT_BUFFERS);
    }

    const unsigned INIT_BUFFERS = 128;
    const size_t BUFSIZE = 512;
    netio::context ctx;
    buffer_feeder feeder;
    reusable_buffer* rb;
    buffer* b;

};


bool cb_called = false;
void callback(netio::message& msg, void* data)
{
    cb_called = true;
    REQUIRE( msg.size() == 7 );
    REQUIRE( msg.num_fragments() == 1 );
    REQUIRE( strcmp((const char*)msg.fragment_list()->data[0], "foobar") == 0 );
    REQUIRE( data == NULL );
}



TEST_CASE( "serialize single message with copy_deserializer / queue", "[deserialization]" )
{
    buf_source source;
    copy_deserializer cd;

    source.append_msg("foobar", 7);
    cd.feed(source.rb);
    netio::message msg;

    CAPTURE( (const char*)msg.fragment_list()->data );
    REQUIRE( cd.try_pop(&msg) == true );
    REQUIRE( msg.size() == 7 );
    REQUIRE( msg.num_fragments() == 1 );
    REQUIRE( strcmp((const char*)msg.fragment_list()->data[0], "foobar") == 0 );
    REQUIRE( source.all_buffers_released() );
}


TEST_CASE( "serialize single message with copy_deserializer / callback", "[deserialization]" )
{
    buf_source source;
    copy_deserializer cd;
    cb_called = false;
    cd.register_cb_on_msg_received(callback, NULL);

    source.append_msg("foobar", 7);
    cd.feed(source.rb);
    netio::message msg;

    REQUIRE( cd.try_pop(&msg) == false );
    REQUIRE( cb_called == true );
    REQUIRE( source.all_buffers_released() );
}


TEST_CASE( "serialize single message with reference_deserializer / queue", "[deserialization]" )
{
    buf_source source;
    reference_deserializer cd;

    source.append_msg("foobar", 7);
    cd.feed(source.rb);
    netio::message msg;

    REQUIRE( cd.try_pop(&msg) == true );
    REQUIRE( msg.size() == 7 );
    REQUIRE( msg.num_fragments() == 1 );
    REQUIRE( strcmp((const char*)msg.fragment_list()->data[0], "foobar") == 0 );
    REQUIRE( source.all_buffers_released() == false );

    std::vector<uint8_t> b(msg.size());
    msg.serialize_to_usr_buffer(b.data());
    REQUIRE( source.all_buffers_released() == true );
}


TEST_CASE( "serialize single message with reference_deserializer / callback", "[deserialization]" )
{
    buf_source source;
    reference_deserializer cd;
    cb_called = false;
    cd.register_cb_on_msg_received(callback, NULL);

    source.append_msg("foobar", 7);
    cd.feed(source.rb);
    netio::message msg;

    REQUIRE( cd.try_pop(&msg) == false );
    REQUIRE( cb_called == true );
    REQUIRE( source.all_buffers_released() == true );
}
