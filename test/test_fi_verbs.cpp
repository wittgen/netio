#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "../src/fi_verbs.hpp"

#include <iostream>
#include <cstdlib>

using namespace netio;


static const char*
ib_localhost_addr()
{
    const char* addr = std::getenv("NETIO_IB_LOCALHOST_ADDR");
    if(!addr)
    {
        addr = "127.0.0.1";
    }
    return addr;
}

TEST_CASE( "create a fi_verbs listen socket", "[fi_verbs]" )
{
    event_loop evloop;
    context ctx("fi_verbs");
    fi_verbs_listen_socket socket(&evloop, endpoint(ib_localhost_addr(), 21344), &ctx);
    socket.listen();
    evloop.run_for(1000);

    REQUIRE( true );
}

TEST_CASE( "connect to a fi_verbs listen socket", "[fi_verbs]" )
{
    event_loop evloop;
    context ctx("fi_verbs");
    fi_verbs_listen_socket listen_socket(&evloop, endpoint(ib_localhost_addr(), 21345), &ctx);
    fi_verbs_send_socket send_socket(&evloop);

    listen_socket.listen();
    send_socket.connect(endpoint(ib_localhost_addr(), 21345));
    evloop.run_for(1000);

    REQUIRE( true );
}


TEST_CASE( "connect and send a message over fi_verbs", "[fi_verbs]" )
{
    event_loop evloop;
    context ctx("fi_verbs");

    std::thread bg_thread([&evloop]()
    {
        evloop.run_forever();
    });

    fi_verbs_listen_socket listen_socket(&evloop, endpoint(ib_localhost_addr(), 21347), &ctx);
    fi_verbs_send_socket send_socket(&evloop);

    listen_socket.listen();
    send_socket.connect(endpoint(ib_localhost_addr(), 21347));
    //evloop.run_for(1000);
    sleep(1);

    reusable_buffer* buffer = new netio::reusable_buffer(128, NULL, &ctx);
    msgheader header;
    header.len = 12;
    buffer->buffer()->append((char*)&header, sizeof(header));
    buffer->buffer()->append("hello world!", 12);

    send_socket.send_buffer(buffer);
    //evloop.run_for(1000);

    sleep(1);

    listen_socket.process_page();
    netio::message msg;
    listen_socket.pop_message_entry(&msg);

    evloop.stop();
    bg_thread.join();

    REQUIRE( msg.size() == 12 );
    REQUIRE( strncmp((char*)msg.fragment_list()->data[0], "hello world!", 12) == 0 );
}


TEST_CASE( "connect and send 1000 messages over fi_verbs", "[fi_verbs]" )
{
    event_loop evloop;
    context ctx("fi_verbs");

    std::thread bg_thread([&evloop]()
    {
        try
        {
            evloop.run_forever();
        }
        catch (std::runtime_error& e)
        {
            std::cerr << "exception in event loop: " << e.what() << std::endl;
        }
    });

    fi_verbs_listen_socket listen_socket(&evloop, endpoint(ib_localhost_addr(), 21348), &ctx);
    fi_verbs_send_socket send_socket(&evloop);

    listen_socket.listen();
    send_socket.connect(endpoint(ib_localhost_addr(), 21348));
    //evloop.run_for(1000);
    sleep(1);

    try
    {
        reusable_buffer* buffer = new netio::reusable_buffer(128, NULL, &ctx);
        msgheader header;
        header.len = 12;
        buffer->buffer()->append((char*)&header, sizeof(header));
        buffer->buffer()->append("hello world!", 12);

        send_socket.send_buffer(buffer);
    }
    catch (std::runtime_error& e)
    {
        std::cerr << "exception in event loop: " << e.what() << std::endl;
    }

    sleep(1);

    listen_socket.process_page();
    netio::message msg;

    for(int i=0; i<1000; i++)
    {
        listen_socket.pop_message_entry(&msg);
        REQUIRE( msg.size() == 12 );
        REQUIRE( strncmp((char*)msg.fragment_list()->data[0], "hello world!", 12) == 0 );
    }

    evloop.stop();
    bg_thread.join();
}
