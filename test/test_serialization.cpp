#include "catch/catch.hpp"

#include "netio/netio.hpp"
#include "../src/serialization.hpp"

using namespace netio;

TEST_CASE( "small buffer", "[serialization]" )
{
    char buffer[1024];
    const char* src = "xxxx";
    msgheader header = {};
    header.len = 4;


    unsigned int offset = 0;
    size_t bytes_written = serialize_header_to_buffer(buffer, sizeof buffer, header);
    bytes_written += serialize_to_buffer(buffer + sizeof header, sizeof buffer, src, 4, &offset);

    REQUIRE ( bytes_written == sizeof(size_t)+4 );
    REQUIRE ( offset == 4 );

    bytes_written = serialize_to_buffer(buffer, sizeof buffer, src, 4, &offset);

    REQUIRE ( bytes_written == 0 );
    REQUIRE ( offset == 4 );

}


TEST_CASE( "buffer too small for header", "[serialization]" )
{
    char buffer[4];
    const char* src = "xxxx";
    msgheader header = {};
    header.len = 4;

    size_t bytes_written = serialize_header_to_buffer(buffer, sizeof buffer, header);

    REQUIRE ( bytes_written == 0 );
}

TEST_CASE( "large buffer", "[serialization]" )
{
    char buffer[12];
    const char* src = "xxxx";
    msgheader header = {};
    header.len = 4;


    unsigned int offset = 0;
    size_t bytes_written = serialize_header_to_buffer(buffer, sizeof buffer, header);
    bytes_written += serialize_to_buffer(buffer+bytes_written, sizeof buffer, src, 4, &offset);

    REQUIRE ( bytes_written == sizeof(size_t)+4 );
    REQUIRE ( offset == 4 );

}

static bool called = false;

void cb(void* msg, size_t size)
{
    called = true;
    REQUIRE( size > 0 );
    REQUIRE( ((char*)msg)[0] == 'x' );
}

TEST_CASE ( "deserialize", "[serialization]" )
{
    deserializer deserializer(&cb);

    msgheader header = { 1024 };
    REQUIRE( deserializer.bytes_consumable() == sizeof header );

    void* buf = deserializer.buffer();
    memcpy(buf, (void*)&header, deserializer.bytes_consumable());
    deserializer.consume(sizeof header);

    REQUIRE( deserializer.bytes_consumable() == 1024 );
    buf = deserializer.buffer();
    char test[] = "xxxxxxxx";
    memcpy(buf, test, 8);
    deserializer.consume(8);

    REQUIRE( deserializer.bytes_consumable() == 1016 );
    deserializer.consume(1016);
    REQUIRE( called == true );
    REQUIRE( deserializer.bytes_consumable() == sizeof header );
}
