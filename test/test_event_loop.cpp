#include "catch/catch.hpp"
#include <unistd.h>
#include "netio/netio.hpp"

using namespace netio;


TEST_CASE( "signal", "[eventloop]" )
{
    bool called = false;

    event_loop evloop;
    signal s(&evloop, [&called](void*)
    {
        called=true;
    }, NULL);

    evloop.run_for(1000);
    REQUIRE( called == false );

    s.fire();
    evloop.run_for(1000);
    REQUIRE( called == true );
}


TEST_CASE( "timer", "[eventloop]" )
{
    bool called = false;

    event_loop evloop;
    timer t(&evloop, [&called](void*)
    {
        called=true;
    }, NULL);

    t.start(100);
    evloop.run_for(1000);

    REQUIRE( called == true );
}


TEST_CASE( "run forever", "[eventloop]" )
{
    bool called = false;

    event_loop evloop;
    timer t(&evloop, [&called](void*)
    {
        called=true;
    }, NULL);

    t.start(100);
    std::thread bg_thread(&event_loop::run_forever, &evloop);

    sleep(1);
    evloop.stop();
    bg_thread.join();

    REQUIRE( called == true );
}

TEST_CASE( "run one", "[eventloop]" )
{
    bool called = false;

    event_loop evloop;
    timer t(&evloop, [&called](void*)
    {
        called=true;
    }, NULL);

    t.start(100);
    evloop.run_one();

    REQUIRE( called == true );
}

TEST_CASE( "cancel run forever", "[eventloop]" )
{
    event_loop evloop;
    std::thread t(&event_loop::run_forever, &evloop);
    sleep(1);
    evloop.stop();
    t.join();
    REQUIRE( true );
}

TEST_CASE("two events first closes fd of second", "[eventloop]")
{
	/* This test reproduces a bug and validates its fix.
	 * With the bug in place, this test would segfault.
	 */
    event_loop evloop;

    signal *s2 = new signal(&evloop, [](void*) {}, NULL);
    signal s1(&evloop, [&s2](void*)
        {
            // simulate a bad case of s2 deletion
            delete s2;
            // delete does not necessarily invalidate the memory
            // so write garbage (but not 0 as there is a test for
            // the validity of the function)
            memset(((char*)s2) + 8, 1, sizeof(netio::context));
        }
        , NULL);

    s1.fire();
    s2->fire();

    evloop.run_one();
}
