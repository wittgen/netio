#include "catch/catch.hpp"
#include "netio/netio.hpp"

using namespace netio;


TEST_CASE( "default socket configuration", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg();
    REQUIRE(cfg.get(sockcfg::BUFFER_PAGES_PER_CONNECTION) == 128);
}

TEST_CASE( "set value with constructor", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg()(sockcfg::BUFFER_PAGES_PER_CONNECTION, 256);
    REQUIRE(cfg.get(sockcfg::BUFFER_PAGES_PER_CONNECTION) == 256);
}

TEST_CASE( "set flag with constructor", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg()(sockcfg::ZERO_COPY);
    REQUIRE(cfg.get(sockcfg::ZERO_COPY));
}

TEST_CASE( "set value", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg()(sockcfg::BUFFER_PAGES_PER_CONNECTION, 256);
    REQUIRE(cfg.get(sockcfg::BUFFER_PAGES_PER_CONNECTION) == 256);
}

TEST_CASE( "set flag", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg()(sockcfg::ZERO_COPY);
    REQUIRE(cfg.get(sockcfg::ZERO_COPY));
}

TEST_CASE( "set multiple flags and settings", "[sockcfg]" )
{
    sockcfg cfg = sockcfg::cfg()(sockcfg::BUFFER_PAGES_PER_CONNECTION, 256)(sockcfg::ZERO_COPY);
    REQUIRE(cfg.get(sockcfg::ZERO_COPY));
    REQUIRE(cfg.get(sockcfg::BUFFER_PAGES_PER_CONNECTION) == 256);
}
