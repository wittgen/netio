#ifndef TEST_UTILITY_HPP
#define TEST_UTILITY_HPP

#include <chrono>
#include <thread>

class Timer
{
public:
    Timer()
        : beg_(clock_::now()) {}

    void reset()
    {
        beg_ = clock_::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_>(clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1>> second_;
    std::chrono::time_point<clock_> beg_;

};

template <typename T>
bool wait_for_condition_timeout(T& condition, double seconds)
{
    using namespace std::chrono;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    while( !condition )
    {
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
        if(time_span.count() > seconds)
            return false;

        std::this_thread::yield();
    }

    return true;
}

const std::vector<std::string> TEST_ALL_BACKENDS = { "libevent", "posix" };

#define FOR_ALL_BACKENDS(PTR) \
  for(auto PTR = std::begin(TEST_ALL_BACKENDS); \
    PTR != std::end(TEST_ALL_BACKENDS);         \
    PTR++)                                  \

#endif
