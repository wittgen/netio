#include "fi_verbs.hpp"
#include "utility.hpp"

#include <sstream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>


#define USE_FI_TRYWAIT

#define MAX_CQ_ENTRIES 4096

#define ERROR(msg, c) \
do { \
	std::stringstream s; \
	s << msg << ": " << fi_strerror(-c) << " (" << -c << ")"; \
	throw std::runtime_error(s.str()); \
} while(0);

//#define TEST_IT
#ifdef TEST_IT
# define DEBUG_LOG( ... ) do { printf("[fi_verbs@%s:%3d] ", __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while(0)
#else
# define DEBUG_LOG( ... )
#endif




union sockaddr_any
{
    struct sockaddr		sa;
    struct sockaddr_in	sin;
    struct sockaddr_in6	sin6;
    struct sockaddr_storage	ss;
};






static int
read_cm_event(struct fid_eq* eq, struct fi_info** info)
{
    uint32_t event;
    struct fi_eq_cm_entry entry;

    ssize_t rd = fi_eq_sread(eq, &event, &entry, sizeof entry, 0, 0);
    if(rd < 0)
    {
        if(rd == -FI_EAGAIN)
        {
            return -1;
        }
        if(rd == -FI_EAVAIL)
        {
            int r;
            struct fi_eq_err_entry err_entry;
            if((r = fi_eq_readerr(eq, &err_entry, 0)) < 0)
            {
                ERROR("fi_eq_readerr failed", r);
            }
            DEBUG_LOG("error in the CQ: %s / %s", fi_strerror(err_entry.err),
                      fi_strerror(err_entry.prov_errno));
            THROW_WITH_MSG(std::runtime_error, "error in the completion queue: " << fi_strerror(err_entry.err)
                           << " / " << fi_strerror(err_entry.prov_errno));
        }
        ERROR("fi_eq_sread failed", rd);
    }
    if (rd != sizeof entry)
    {
        ERROR("reading from event queue failed: rd=%d", rd);
    }

    if(info != NULL)
        *info = entry.info;

    return event;
}


netio::fi_verbs_listen_socket::fi_verbs_listen_socket(event_loop* evloop, netio::endpoint endpoint,
                                                      netio::context* c, sockcfg cfg)
    : netio::backend_listen_socket(evloop, endpoint, c, cfg)
    , fi(nullptr)
    , fabric(nullptr)
    , eq(nullptr)
    , pep(nullptr)
{
}


netio::fi_verbs_listen_socket::~fi_verbs_listen_socket()
{
}


void
netio::fi_verbs_process_recv_socket_cq_event(int fd, void* data)
{
    struct fi_cq_msg_entry completion_entry;
    struct fi_verbs_recv_socket * socket = (fi_verbs_recv_socket*)data;

    do
    {
        while(true)
        {
            DEBUG_LOG("--");
            int ret = fi_cq_read(socket->cq, &completion_entry, 1);
            if(ret < 0)
            {
                if(ret == -FI_EAGAIN)
                    break;
                if(ret == -FI_EAVAIL)
                {
                    int r;
                    struct fi_cq_err_entry err_entry;
                    if((r = fi_cq_readerr(socket->cq, &err_entry, 0)) < 0)
                    {
                        ERROR("fi_eq_readerr failed", r);
                    }
                    DEBUG_LOG("error in the CQ: %s / %s", fi_strerror(err_entry.err),
                              fi_strerror(err_entry.prov_errno));

                    char eb[1024];
                    THROW_WITH_MSG(std::runtime_error, "error in the completion queue: " << fi_strerror(err_entry.err)
                                   << " / " << fi_strerror(err_entry.prov_errno)
                                   << ": " << fi_cq_strerror(socket->cq, err_entry.prov_errno, err_entry.err_data, eb, 1024));
                }
                ERROR("fi_cq_read failed", ret);
            }
            if(ret > 0)
            {
                netio::reusable_buffer* buffer = (netio::reusable_buffer*)completion_entry.op_context;
                buffer->buffer()->advance(completion_entry.len);
#ifdef TEST_IT
                fi_verbs_buffer* buf = (fi_verbs_buffer*)buffer->buffer()->backend_buffer();
                struct fid_mr* mr = buf->mr;
                char* page = buffer->buffer()->data();
#endif
                DEBUG_LOG("message received: (size=%d)", completion_entry.len);
                DEBUG_LOG("mr received: 0x%x", mr);
                DEBUG_LOG("add page entry 0x%llx", page);

                socket->listen_socket->add_page_entry(buffer, completion_entry.len, socket);
                //buffer->release();

                socket->post_recv();
            }
        }


#ifdef USE_FI_TRYWAIT
        fi_verbs_listen_socket* listen_socket = (fi_verbs_listen_socket*)socket->listen_socket;

        struct fid* fids = &socket->cq->fid;
        if (fi_trywait(listen_socket->fabric, &fids, 1) == FI_SUCCESS)
        {
            DEBUG_LOG("fi_trywait: true, can wait");
            break;
        }
        else
        {
            DEBUG_LOG("fi_trywait: false");
            //throw std::runtime_error("need to fix this");
        }
#else
        break;
#endif
    }
    while(true);
}


void
netio::fi_verbs_process_recv_socket_cm_event(int fd, void* data)
{
    netio::fi_verbs_recv_socket* socket = (netio::fi_verbs_recv_socket*)data;

    int ret;
    uint32_t event = read_cm_event(socket->eq, NULL);

    switch (event)
    {
    case FI_CONNECTED:
        DEBUG_LOG("fi_verbs_process_recv_socket_cm_event: FI_CONNECTED");

        if((ret = fi_control(&socket->cq->fid, FI_GETWAIT, &socket->ev_context.fd)))
        {
            ERROR("fi_control failed", ret);
        }

        socket->ev_context.data = socket;
        socket->ev_context.fn = fi_verbs_process_recv_socket_cq_event;
        socket->evloop->register_read_fd(&socket->ev_context);
        for(uint64_t i=0; i<socket->BUFFER_PAGES; i++)
            socket->post_recv();

        socket->status = fi_verbs_recv_socket::OPEN;
        break;

    case FI_SHUTDOWN:
        DEBUG_LOG("fi_verbs_process_recv_socket_cm_event: FI_SHUTDOWN");

        socket->status = fi_verbs_recv_socket::CLOSED;

        close(socket->ev_context_cm.fd);
        close(socket->ev_context.fd);

        if((ret = fi_close(&socket->ep->fid)))
        {
            ERROR("fi_close (ep) failed", ret)
        }

        if((ret = fi_close(&socket->cq->fid)))
        {
            ERROR("fi_close (cq) failed", ret)
        }

        if((ret = fi_close(&socket->eq->fid)))
        {
            ERROR("fi_close (eq) failed", ret)
        }

        DEBUG_LOG("Clearing all pages now in recv socket");
        socket->feeder.clear_all_pages();

        if((ret = fi_close(&socket->domain->fid)))
        {
            ERROR("fi_close (domain) failed", ret)
        }

        DEBUG_LOG("shutdown complete in recv socket");

        break;

    case -1:
        // Do nothing, we go FI_EAGAIN
        break;

    default:
        throw std::runtime_error("this is not the event we are looking for");
        break;
    }
}


void
netio::fi_verbs_process_listen_socket_cm_event(int fd, void* data)
{
    netio::fi_verbs_listen_socket* lsocket = (netio::fi_verbs_listen_socket*)data;

    struct fi_info *info = NULL;
    int ret;
    netio::fi_verbs_recv_socket * rsocket = NULL;
    struct fi_eq_attr eq_attr;
    eq_attr.wait_obj = FI_WAIT_FD;

    DEBUG_LOG("handling incoming connection request");

    int event = read_cm_event(lsocket->eq, &info);

    switch (event)
    {
    case FI_CONNREQ:
        DEBUG_LOG("fi_verbs_process_listen_socket_cm_event: FI_CONNREQ");

        rsocket = new netio::fi_verbs_recv_socket(lsocket->evloop, lsocket);

        if((ret = fi_domain(lsocket->fabric, info, &rsocket->domain, NULL)))
        {
            ERROR("fi_domain failed", ret);
        }

        if((ret = fi_endpoint(rsocket->domain, info, &rsocket->ep, NULL)))
        {
            ERROR("fi_endpoint failed", ret);
        }

        /* Create a new event queue for the new active socket */
        if((ret = fi_eq_open(lsocket->fabric, &eq_attr, &rsocket->eq, NULL)))
        {
            ERROR("fi_eq_open failed", ret);
        }

        if((ret = fi_ep_bind((rsocket->ep), &rsocket->eq->fid, 0)))
        {
            ERROR("fi_ep_bind failed", ret);
        }

        if((ret = fi_control(&rsocket->eq->fid, FI_GETWAIT, &rsocket->ev_context_cm.fd)))
        {
            ERROR("fi_control failed", ret);
        }
        rsocket->ev_context_cm.data = rsocket;
        rsocket->ev_context_cm.fn = fi_verbs_process_recv_socket_cm_event;
        rsocket->evloop->register_read_fd(&rsocket->ev_context_cm);

        struct fi_cq_attr cq_attr;
        cq_attr.size = MAX_CQ_ENTRIES;      /* # entries for CQ */
        cq_attr.flags = 0;     /* operation flags */
        cq_attr.format = FI_CQ_FORMAT_MSG;    /* completion format */
        cq_attr.wait_obj= FI_WAIT_FD;  /* requested wait object */
        cq_attr.signaling_vector = 0; /* interrupt affinity */
        cq_attr.wait_cond = FI_CQ_COND_NONE; /* wait condition format */
        cq_attr.wait_set = NULL;  /* optional wait set */

        if((ret = fi_cq_open(rsocket->domain, &cq_attr, &rsocket->cq, NULL)))
        {
            ERROR("fi_cq_open failed", ret);
        }

        if((ret = fi_ep_bind((rsocket->ep), &rsocket->cq->fid, FI_TRANSMIT|FI_RECV)))
        {
            ERROR("fi_ep_bind failed", ret);
        }

        if((ret = fi_enable(rsocket->ep)))
        {
            ERROR("fi_enable failed", ret);
        }

        if((ret = fi_accept(rsocket->ep, NULL, 0)))
        {
            ERROR("fi_accept failed", ret);
        }
        DEBUG_LOG("connection accepted");
        break;

    case FI_CONNECTED:
        throw std::runtime_error("FI_CONNECTED received on an fi_verbs listen socket");
        break;

    case FI_SHUTDOWN:
        throw std::runtime_error("FI_SHUTDOWN received on an fi_verbs listen socket");
        break;

    case -1:
        // Do nothing, we go FI_EAGAIN
        break;
    }
}


void
netio::fi_verbs_listen_socket::listen()
{
    int ret;
    struct fi_info* hints;
    struct fi_eq_attr eq_attr;
    eq_attr.wait_obj = FI_WAIT_FD;

    hints = fi_allocinfo();
    hints->ep_attr->type  = FI_EP_MSG;
    hints->caps = FI_MSG;
    hints->mode = FI_LOCAL_MR;
    char port_addr[32];
    snprintf(port_addr, 32, "%d", this->ep.port());

    DEBUG_LOG("listening on %s:%s", this->ep.address().c_str(), port_addr);

    const char* addr = NULL;
    if(this->ep.address() != "0.0.0.0")
      {
	addr = this->ep.address().c_str();
      }

    if((ret = fi_getinfo(FI_VERSION(1, 1), addr, port_addr, FI_SOURCE, hints,
                         &this->fi)))
    {
        ERROR("fi_getinfo failed", ret);
    }

    if((ret = fi_fabric(this->fi->fabric_attr, &this->fabric, NULL)))
    {
        ERROR("fi_fabric failed", ret);
    }

    if((ret = fi_eq_open(this->fabric, &eq_attr, &this->eq, NULL)))
    {
        ERROR("fi_eq_open failed", ret);
    }

    if((ret = fi_passive_ep(this->fabric, this->fi, &this->pep, NULL)))
    {
        ERROR("fi_passive_ep failed", ret);
    }

    if((ret = fi_pep_bind(this->pep, &this->eq->fid, 0)))
    {
        ERROR("fi_pep_bind failed", ret);
    }

    if((ret = fi_listen(this->pep)))
    {
        ERROR("fi_listen failed", ret);
    }

    if((ret = fi_control(&this->eq->fid, FI_GETWAIT, &this->ev_context.fd)))
    {
        ERROR("fi_control failed", ret);
    }

    ev_context.data = this;
    ev_context.fn = fi_verbs_process_listen_socket_cm_event;
    this->evloop->register_read_fd(&ev_context);
}


static const char *
sockaddrstr(const union sockaddr_any *addr, socklen_t len, char *buf, size_t buflen,
            unsigned short* port)
{
    static char namebuf[BUFSIZ];
    static char servbuf[BUFSIZ];
    int errcode;

    if ((errcode = getnameinfo(&addr->sa, len, namebuf, BUFSIZ,
                               servbuf, BUFSIZ,
                               NI_NUMERICHOST | NI_NUMERICSERV)))
    {
        std::stringstream s;
        if (errcode != EAI_SYSTEM)
        {
            s << "getnameinfo: " << gai_strerror(errcode);
            throw std::runtime_error(s.str());
        }
        else
        {
            s << "getnameinfo: " << strerror(errno);
            throw std::runtime_error(s.str());
        }
    }

    snprintf(buf, buflen, "%s", namebuf);
    *port = atoi(servbuf);
    return buf;
}

netio::endpoint
netio::fi_verbs_listen_socket::endpoint() const
{
    union sockaddr_any addr;
    memset(&addr, 0, sizeof(addr));
    size_t addrlen = sizeof(addr);

    int ret;
    if((ret = fi_getname(&this->pep->fid, &addr, &addrlen)))
    {
        DEBUG_LOG("error fi_getname: %s", fi_strerror(-ret));
        ERROR("fi_getname failed", ret);
    }


    char buf[128];
    unsigned short port;
    const char* straddr = sockaddrstr(&addr, addrlen, buf, 128, &port);

    DEBUG_LOG("endpoint() -> %s:%d", straddr, port);
    return netio::endpoint(straddr, port);
}


netio::fi_verbs_recv_socket::fi_verbs_recv_socket(event_loop* evloop, backend_listen_socket* ls)
    : backend_recv_socket(evloop, ls)
    , domain(nullptr)
    , ep(nullptr)
    , eq(nullptr)
    , cq(nullptr)
    , endpoint_is_cached(false)
{
}


netio::fi_verbs_recv_socket::~fi_verbs_recv_socket()
{
}


void
netio::fi_verbs_recv_socket::post_recv()
{
    if(status == fi_verbs_recv_socket::CLOSED)
        return;

    int ret;
    netio::reusable_buffer* buffer;
    // TODO: This should be replaced with an approach that does not use busy waiting
    while(false == try_fetch_page(&buffer))
    {
        std::this_thread::yield();
    }
    fi_verbs_buffer* buf = (fi_verbs_buffer*)buffer->buffer()->backend_buffer();

    if(!buf->mr)
    {
        if((ret = fi_mr_reg(this->domain, buf->buffer(), buf->size(), FI_RECV, 0, req_key++, 0, &buf->mr,
                            NULL)))
        {
            DEBUG_LOG("requested key: %d", req_key);
            ERROR("fi_mr_reg failed", ret);
        }
    }

    if((ret = fi_recv(this->ep, buf->buffer(), buf->size(), fi_mr_desc(buf->mr), 0, buffer)))
    {
        DEBUG_LOG("requested key: %d", req_key);
        ERROR("fi_recv failed", ret);
    }
}


netio::endpoint
netio::fi_verbs_recv_socket::remote_endpoint()
{
    if(!endpoint_is_cached)
    {
        union sockaddr_any addr;
        memset(&addr, 0, sizeof(addr));
        size_t addrlen = sizeof(addr);

        int ret;
        if((ret = fi_getpeer(this->ep, &addr, &addrlen)))
        {
            ERROR("fi_getpeer failed", ret);
        }

        char buf[128];
        unsigned short port;
        const char* straddr = sockaddrstr(&addr, addrlen, buf, 128, &port);

        DEBUG_LOG("remote endpoint() -> %s:%d", straddr, port);

        netio::endpoint tmp(straddr, port);
        cached_endpoint = std::move(tmp);
        endpoint_is_cached = true;
    }
    return cached_endpoint;
}


netio::fi_verbs_send_socket::fi_verbs_send_socket(event_loop* evloop, sockcfg cfg)
    : netio::backend_send_socket(evloop, cfg)
    , fi(nullptr)
    , fabric(nullptr)
    , eq(nullptr)
    , domain(nullptr)
    , ep(nullptr)
    , cq(nullptr)
{
    outstanding_completions.store(0);
    char* env_windowsize = getenv("NETIO_WINDOWSIZE");
    if(!env_windowsize)
    {
        windowsize.store(8);
    }
    else
    {
        windowsize.store(atoi(env_windowsize));
    }
    current_window.store(0);
}


netio::fi_verbs_send_socket::~fi_verbs_send_socket()
{
    int ret;

    if(this->domain && (ret = fi_close(&this->domain->fid)))
    {
        DEBUG_LOG("warning: fi_close (domain) failed in send_socket destructor: %d", ret);
    }

    if(this->fabric && (ret = fi_close(&this->fabric->fid)))
    {
        DEBUG_LOG("warning: fi_close (fabric) failed in send_socket destructor: %d", ret);
    }
}


void
netio::fi_verbs_send_socket::connect(const endpoint& ep)
{
    int ret;
    struct fi_info* hints;
    struct fi_eq_attr eq_attr;
    eq_attr.wait_obj = FI_WAIT_UNSPEC; // waiting only through fi_ calls (no epoll)

    hints = fi_allocinfo();
    hints->ep_attr->type  = FI_EP_MSG;
    hints->caps = FI_MSG;
    hints->mode   = FI_LOCAL_MR;
    hints->domain_attr->data_progress = FI_PROGRESS_AUTO;
    hints->domain_attr->resource_mgmt = FI_RM_ENABLED;

    char port_addr[32];
    snprintf(port_addr, 32, "%d", ep.port());

    DEBUG_LOG("connecting to endpoint %s:%d", ep.address().c_str(), ep.port());

    if((ret = fi_getinfo(FI_VERSION(1, 1), ep.address().c_str(), port_addr, 0, hints, &this->fi)))
    {
        ERROR("fi_getinfo failed", ret);
    }

    if((ret = fi_fabric(this->fi->fabric_attr, &this->fabric, NULL)))
    {
        ERROR("fi_fabric failed", ret);
    }

    if((ret = fi_eq_open(this->fabric, &eq_attr, &this->eq, NULL)))
    {
        ERROR("fi_eq_open failed", ret);
    }

    if((ret = fi_domain(this->fabric, this->fi, &this->domain, NULL)))
    {
        ERROR("fi_domain failed", ret);
    }

    if((ret = fi_endpoint(this->domain, this->fi, &this->ep, NULL)))
    {
        ERROR("fi_endpoint failed", ret);
    }

    if((ret = fi_ep_bind((this->ep), &this->eq->fid, 0)))
    {
        ERROR("fi_ep_bind failed", ret);
    }

    struct fi_cq_attr cq_attr;
    cq_attr.size = MAX_CQ_ENTRIES;      /* # entries for CQ */
    cq_attr.flags = 0;     /* operation flags */
    cq_attr.format = FI_CQ_FORMAT_CONTEXT;    /* completion format */
    cq_attr.wait_obj= FI_WAIT_UNSPEC;  /* requested wait object */
    cq_attr.signaling_vector = 0; /* interrupt affinity */
    cq_attr.wait_cond = FI_CQ_COND_NONE; /* wait condition format */
    cq_attr.wait_set = NULL;  /* optional wait set */

    if((ret = fi_cq_open(this->domain, &cq_attr, &this->cq, NULL)))
    {
        ERROR("fi_cq_open failed", ret);
    }

    if((ret = fi_ep_bind((this->ep), &this->cq->fid, FI_TRANSMIT|FI_RECV)))
    {
        ERROR("fi_ep_bind failed", ret);
    }

    /* Connect to server */
    if((ret = fi_connect(this->ep, this->fi->dest_addr, NULL, 0)))
    {
        ERROR("fi_connect failed", ret);
    }

    if((ret = fi_control(&this->eq->fid, FI_GETWAIT, &this->ev_context_cm.fd)))
    {
        ERROR("fi_control failed", ret);
    }

    ev_context_cm.data = this;
    ev_context_cm.fn = fi_verbs_process_send_socket_cm_event;
    this->evloop->register_read_fd(&ev_context_cm);

    status = backend_send_socket::CONNECTING;

    DEBUG_LOG("send socket: waiting for connection");
}


void
netio::fi_verbs_process_send_socket_cq_event(int fd, void* ptr)
{
    DEBUG_LOG("processing send socket cq event");
    struct fi_cq_msg_entry completion_entry;
    struct fi_verbs_send_socket * socket = (fi_verbs_send_socket*)ptr;

    do
    {
        while(true)
        {
            int ret = fi_cq_read(socket->cq, &completion_entry, 1);
            if(ret < 0)
            {
                if(ret == -FI_EAGAIN)
                    break;
                if(ret == -FI_EAVAIL)
                {
                    int r;
                    struct fi_cq_err_entry err_entry;
                    if((r = fi_cq_readerr(socket->cq, &err_entry, 0)) < 0)
                    {
                        ERROR("fi_eq_readerr failed", r);
                    }
                    DEBUG_LOG("error in the CQ: %s / %s", fi_strerror(err_entry.err),
                              fi_strerror(err_entry.prov_errno));
                    char eb[1024];
                    THROW_WITH_MSG(std::runtime_error, "error in the completion queue: " << fi_strerror(err_entry.err)
                                   << " / " << fi_strerror(err_entry.prov_errno)
                                   << ": " << fi_cq_strerror(socket->cq, err_entry.prov_errno, err_entry.err_data, eb, 1024));
                }
                ERROR("fi_cq_read failed", ret);
            }
            DEBUG_LOG("send completed");
            netio::reusable_buffer* buf = (netio::reusable_buffer*)completion_entry.op_context;
            buf->release();
            socket->outstanding_completions--;
        }

#ifdef USE_FI_TRYWAIT
        struct fid* fids = &socket->cq->fid;
        if (fi_trywait(socket->fabric, &fids, 1) == FI_SUCCESS)
        {
            DEBUG_LOG("fi_trywait: true, can wait");
            break;
        }
        else
        {
            DEBUG_LOG("fi_trywait: false");
            //throw std::runtime_error("need to fix this");
        }
#else
        break;
#endif
    }
    while(true);
}


void
netio::fi_verbs_process_send_socket_cm_event(int fd, void* ptr)
{
    fi_verbs_send_socket* socket = (fi_verbs_send_socket*)ptr;

    int event = read_cm_event(socket->eq, NULL);
    int ret;

    switch(event)
    {
    case FI_SHUTDOWN:
        DEBUG_LOG("send socket: disconnected");
        socket->status = backend_send_socket::CLOSED;

        if((ret = fi_close(&socket->ep->fid)))
        {
            ERROR("fi_close (ep) failed", ret);
        }

        if((ret = fi_close(&socket->cq->fid)))
        {
            ERROR("fi_close (cq) failed", ret);
        }

        if((ret = fi_close(&socket->eq->fid)))
        {
            ERROR("fi_close (eq) failed", ret);
        }

        return;

    case FI_CONNECTED:
        socket->ev_context.data = socket;
        socket->ev_context.fn = fi_verbs_process_send_socket_cq_event;
        //socket->evloop->register_read_fd(&socket->ev_context);

        DEBUG_LOG("send socket: connected");
        if(socket->on_connection_opened)
            socket->on_connection_opened();
        socket->status = backend_send_socket::OPEN;
        return;

    default:
        throw std::runtime_error("unexpected event");
    }
}


void
netio::fi_verbs_send_socket::disconnect()
{
    DEBUG_LOG("send socket: disconnect / outstanding completions: %d", outstanding_completions.load());
    while(outstanding_completions.load() > 0)
    {
        check_for_completions();
        usleep(10);
    }

    int ret;
    if(ep)
    {
        if((ret = fi_shutdown(ep, 0)))
        {
            ERROR("fi_shutdown failed", ret)
        }
    }
    status = CLOSED;
    if(on_connection_closed)
        on_connection_closed();
}

void
netio::fi_verbs_send_socket::handle_completion(fi_cq_msg_entry* entry)
{
    netio::reusable_buffer* buf = (netio::reusable_buffer*)entry->op_context;
    buf->release();
    outstanding_completions--;
    DEBUG_LOG("outstanding completions: %d", outstanding_completions.load());
    DEBUG_LOG("handling completion");
}


void
netio::fi_verbs_send_socket::handle_cq_error()
{
    int r;
    struct fi_cq_err_entry err_entry;
    if((r = fi_cq_readerr(cq, &err_entry, 0)) < 0)
    {
        ERROR("fi_eq_readerr failed", r);
    }
    THROW_WITH_MSG(std::runtime_error, "error in the CQ: "
                   <<fi_strerror(err_entry.err) << " / " << fi_strerror(err_entry.prov_errno));
}


int
netio::fi_verbs_send_socket::check_for_completions()
{
    fi_cq_msg_entry entry;
    ssize_t num_completions = fi_cq_read(cq, &entry, 1);
    if(num_completions < 0)
    {
        if(num_completions == -FI_EAGAIN)
            return 0;
        if(num_completions == -FI_EAVAIL)
            handle_cq_error();
    }

    handle_completion(&entry);

    //  DEBUG_LOG("check_for_completions: %d", num_completions);

    return num_completions;
}


void
netio::fi_verbs_send_socket::wait_for_completions(unsigned num)
{
    if(num==0)
        return;

    DEBUG_LOG("Wait for %d completions", num);

    fi_cq_msg_entry entry;

    unsigned completions_arrived = 0;
    while(completions_arrived < num)
    {
        if (status == CLOSED)
        {
            DEBUG_LOG("Connection closed. Aborting wait.");
            break;
        }
        ssize_t num_completions = fi_cq_read(cq, &entry, 1);
        //DEBUG_LOG("wait_for_completions: num_completions: %d", num_completions);
        if(num_completions < 0)
        {
            if(num_completions == -FI_EAGAIN)
                continue;
            if(num_completions == -FI_EAVAIL)
                handle_cq_error();
        }
        else if(num_completions == 1)
        {
            handle_completion(&entry);
            completions_arrived += num_completions;
        }
        else
        {
            THROW_WITH_MSG(std::runtime_error, "Got more completions then we asked for");
        }
    }

    DEBUG_LOG("waiting done");
}


void
netio::fi_verbs_send_socket::send_buffer(netio::reusable_buffer* buffer)
{
    fi_verbs_buffer* buf = (fi_verbs_buffer*)buffer->buffer()->backend_buffer();


    int ret;
    char* d = buffer->buffer()->data();
    size_t s = buffer->buffer()->pos();

    // TODO: this should move maybe to the fi_verbs_buffer constructor?
    if(!(buf->mr))
    {
        if((ret = fi_mr_reg(this->domain, d, buffer->buffer()->size(), FI_SEND, 0, req_key++, 0, &buf->mr,
                            NULL)))
        {
            DEBUG_LOG("could not register mr");
            ERROR("fi_mr_reg failed", ret);
        }
    }

    void* desc = fi_mr_desc(buf->mr);


    while(true)
    {
        if((ret = fi_send(this->ep, d, s, desc, 0, buffer)))
        {
            if(ret == -FI_EAGAIN)
            {
                DEBUG_LOG("FI_EAGAIN");
                continue;
            }
            else
            {
                ERROR("fi_send failed", ret);
            }
        }
        break;
    }

    current_window++;
    outstanding_completions++;
    DEBUG_LOG("sent!");

    if(current_window >= windowsize)
    {
        wait_for_completions(current_window);
        current_window = 0;
    }
    int num_completions = check_for_completions();
    current_window -= num_completions;
}


netio::fi_verbs_buffer::fi_verbs_buffer(size_t s)
    : netio::backend_buffer(s)
{
    mr = NULL;
}


netio::fi_verbs_buffer::~fi_verbs_buffer()
{
	int ret;
	if(mr)
	{
			DEBUG_LOG("Closing MR");
			if((ret = fi_close(&mr->fid)))
			{
					DEBUG_LOG("fi_close failed: %d", ret);
			}
			mr = nullptr;
	}
}


void
netio::fi_verbs_buffer::alloc()
{
    ptr = new uint8_t[size_];
}


netio::backend_send_socket*
netio::fi_verbs_backend::make_send_socket(event_loop* evloop, sockcfg cfg)
{
    return new fi_verbs_send_socket(evloop, cfg);
}


netio::backend_listen_socket*
netio::fi_verbs_backend::make_listen_socket(event_loop* evloop, endpoint ep, netio::context* c,
                                            sockcfg cfg)
{
    return new fi_verbs_listen_socket(evloop, ep, c, cfg);
}


netio::backend_buffer*
netio::fi_verbs_backend::make_buffer(size_t size)
{
    return new fi_verbs_buffer(size);
}
