#pragma once

#include "backend.hpp"

namespace netio
{

class posix_send_socket : public backend_send_socket
{
public:
    posix_send_socket(event_loop* evloop, sockcfg cfg = sockcfg::cfg());
    virtual ~posix_send_socket();

    virtual void connect(const endpoint& ep);
    virtual void disconnect();
    virtual void send_buffer(netio::reusable_buffer* buffer);
};


class posix_listen_socket : public backend_listen_socket
{
public:
    posix_listen_socket(event_loop* evloop, netio::endpoint endpoint, netio::context* c,
                        sockcfg cfg = sockcfg::cfg());
    virtual ~posix_listen_socket();

    virtual void listen();
    virtual netio::endpoint endpoint() const;

protected:
    void accept_connections();
};


class posix_recv_socket : public backend_recv_socket
{
public:
    posix_recv_socket(event_loop* evloop, backend_listen_socket* ls, int fd);
    virtual ~posix_recv_socket();

    netio::endpoint remote_endpoint();

    friend void posix_process_incoming_data(posix_recv_socket* socket);
    friend void posix_end_processing_and_close_socket(posix_recv_socket* socket);

private:
    netio::reusable_buffer* current_page;

    netio::endpoint cached_endpoint;
    bool endpoint_is_cached;
};


void posix_process_incoming_data(posix_recv_socket* socket);
// assumes that if socket->current_page is not null, the socket owns the page,
// i.e. refcount was incremented when fetched.
void posix_end_processing_and_close_socket(netio::posix_recv_socket* socket);

class posix_backend : public backend
{
    backend_send_socket* make_send_socket(event_loop* evloop, sockcfg cfg = sockcfg::cfg());
    backend_listen_socket* make_listen_socket(event_loop* evloop, endpoint ep, netio::context* c,
                                              sockcfg cfg = sockcfg::cfg());
};


}
