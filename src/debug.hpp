#pragma once

#include <execinfo.h>
#include <iostream>

#warning "Compiling with debug header"

#define BACKTRACE_SIZE (32)

inline void dump_stacktrace(const char* message)
{
    void *array[BACKTRACE_SIZE];
    size_t size;
    char **strings;
    size_t i;

    size = backtrace (array, BACKTRACE_SIZE);
    strings = backtrace_symbols (array, size);

    printf ("Obtained %zd stack frames.\n", size);
    std::cout << "Backtrace '" << message << "'" << std::endl;

    for (i = 0; i < size; i++)
        std::cout << "  " << strings[i] << std::endl;

    free (strings);
}
