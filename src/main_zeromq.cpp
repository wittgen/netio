#include "zmq.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>


const char* HELP =
    R"FOO(Usage: netio_zeromq [options]

Uses ZeroMQ to perform throughput measurements to compare to netio.

Options are:

  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -m BYTES        The size of a message that is transferred in bytes.
                  Default: 1024.
  -s MEGABYTES    The total amount of megabytes that is transferred.
                  Default: 1024 megabyte (1 GB).
  -t THREADS      The number of threads to send messages. Default: 1.
  -h              Display this help message.
)FOO";

void run(unsigned long long n_messages, size_t msgsize, const char* host, unsigned short port)
{
    zmq::context_t context (1);
    zmq::socket_t  sender(context, ZMQ_PUSH);

    zmq::socket_t socket(context, ZMQ_PUSH);
    std::stringstream stream;
    stream << "tcp://" << host << ":" << port;
    socket.connect(stream.str().c_str());

    for(unsigned int i=0; i<n_messages; i++)
    {
        zmq::message_t message(msgsize);
        socket.send(message);
    }
}


int main (int argc, char *argv[])
{
    size_t msgsize = 1024;
    size_t transfer_bytes = 1024*1024*1024; // 1 GB
    unsigned short port = 12345;
    std::string host = "127.0.0.1";
    unsigned n_threads = 1;

    char opt;
    while ((opt = getopt(argc, argv, "H:p:m:s:t:h")) != -1)
    {
        switch (opt)
        {
        case 'H':
            host = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'm':
            msgsize = atoi(optarg);
            break;
        case 's':
            transfer_bytes = std::stoull(optarg)*1024*1024;
            break;
        case 't':
            n_threads = atoi(optarg);
            break;
        case 'h':
        default:
            std::cerr << HELP << std::endl;
            return -1;
        }
    }

    unsigned long long n_messages = transfer_bytes/msgsize;

    typedef std::chrono::high_resolution_clock clock;
    auto t0 = clock::now();

    std::vector<std::thread> threads;
    for(unsigned i=0; i<n_threads; i++)
    {
        threads.emplace_back(run, n_messages, msgsize, host.c_str(), port/*+i*/);
    }
    for(unsigned i=0; i<n_threads; i++)
    {
        threads[i].join();
    }


    auto t1 = clock::now();
    double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
    double GBps = (double)(n_threads*n_messages*msgsize)/(1024*1024*1024)/seconds;
    double Gbps = GBps*8;
    std::cout << "Remote host         : " << host << ":" << port << std::endl;
    std::cout << "Message size [Byte] : " << msgsize << std::endl;
    std::cout << "Number of messages  : " << n_messages*n_threads << std::endl;
    std::cout << "Total transfer [GB] : " << (n_threads*n_messages*msgsize)/(1024.*1024*1024) << std::endl;
    std::cout << "Time [s]            : " << seconds << std::endl;
    std::cout << "Throughput [GB/s]   : " << GBps << std::endl;
    std::cout << "Throughput [Gb/s]   : " << Gbps << std::endl;

    return 0;
}
