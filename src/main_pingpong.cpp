#include <iostream>
#include <chrono>
#include <unistd.h>

#include "netio/netio.hpp"


const char* HELP =
    R"FOO(Usage: netio_pingpong client [options]
       netio_pingpong server [options]

A NetIO ping pong application to measure network RTT (round-trip time) using
low-latency sockets.

Options are:

  -b BACKEND      Use the specified netio backend. Default: posix.
  -H HOSTNAME     Connect to the specified hostname. Default: 127.0.0.1.
  -p PORT         Connect to the specified port on the remote. Default: 12345.
  -P PORT         Listen for incoming ping/pong message on the specified port. Default: 12345.
  -h              Display this help message.
)FOO";


struct ping_callback
{
	typedef std::chrono::high_resolution_clock clock;
	clock::time_point t0, t1;
	netio::low_latency_send_socket& socket;

	ping_callback(netio::low_latency_send_socket& socket) : socket(socket)
	{}

	void operator()(netio::endpoint& ep, netio::message& msg)
	{

		std::cout << ">";
    for(const netio::message::fragment* p = msg.fragment_list(); p != nullptr; p = p->next)
    {
        std::cout << " " << p->data[0];
        if(p->data[1])
          std::cout << " " << p->data[1];
    }
    std::cout << std::endl;

		if(strcmp((const char*)msg.fragment_list()->data[0], "connect") == 0)
		{
			t0 = clock::now();
			socket.send(netio::message((uint8_t*)"ping", 5));
		}
		else
		{
			t1 = clock::now();
			double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()/1000000.;
			std::cout << seconds << std::endl;

			t0 = clock::now();
			socket.send(netio::message((uint8_t*)"ping", 5));
		}
	}
};

static void
ping(const char* host,
     unsigned short port,
     unsigned short port_listen,
     netio::context* ctx)
{
	netio::low_latency_send_socket send(ctx);
	ping_callback cb(send);
	netio::low_latency_recv_socket recv(ctx, port_listen, cb);
	send.connect(netio::endpoint(host, port));
	sleep(1);
	send.send(netio::message((uint8_t*)"connect", 8));

	while(true)
		usleep(10);
}


struct
pong_callback
{
	netio::low_latency_send_socket send;
	bool connected;
	std::string host;
	unsigned short port;

	pong_callback(netio::context* ctx, std::string host, unsigned short port)
	: send(ctx), connected(false), host(host), port(port)
	{}

	void operator()(netio::endpoint& ep, netio::message& msg)
	{
    std::cout << ">";
    for(const netio::message::fragment* p = msg.fragment_list(); p != nullptr; p = p->next)
    {
        std::cout << " " << p->data[0];
        if(p->data[1])
          std::cout << " " << p->data[1];
    }
    std::cout << std::endl;
    if(strcmp((const char*)msg.fragment_list()->data[0], "connect") == 0)
		{
			send.connect(netio::endpoint(host, port));
			sleep(2);
			send.send(netio::message((uint8_t*)"connect", 8));
			connected = true;
		}
		else
		{
			send.send(netio::message((uint8_t*)"pong", 5));
		}
	}
};

static void
pong(const char* host,
     unsigned short port,
     unsigned short port_listen,
     netio::context* ctx)
{
	pong_callback cb(ctx, host, port);
	netio::low_latency_recv_socket recv(ctx, port_listen, cb);

	while(true)
		usleep(10);
}


int
main(int argc, char** argv)
{
	unsigned short port_listen = 12345;
	unsigned short port = 12345;
	std::string host = "127.0.0.1";
	std::string backend = "posix";

	char opt;
	while ((opt = getopt(argc, argv, "b:H:p:P:h")) != -1)
	{
		switch (opt)
		{
		case 'b':
			backend = optarg;
			break;
		case 'H':
			host = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'P':
			port_listen = atoi(optarg);
			break;
		case 'h':
		default:
			std::cerr << HELP << std::endl;
			return -1;
		}
	}

	if(optind >= argc)
	{
		std::cerr << HELP << std::endl;
		return -1;
	}

	netio::context ctx(backend.c_str());
	std::thread bg_thread([&ctx](){
		ctx.event_loop()->run_forever();
	});

	if(strcmp(argv[optind], "server") == 0)
	{
		pong(host.c_str(), port, port_listen, &ctx);
	}
	else if(strcmp(argv[optind], "client") == 0)
	{
		ping(host.c_str(), port, port_listen, &ctx);
	}
	else
	{
		std::cerr << "Unknown command: " << argv[optind] << std::endl;
		std::cerr << HELP << std::endl;
		return -1;
	}

	ctx.event_loop()->stop();
	bg_thread.join();
	return 0;
}
